function init() {

    var loginUser;
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var post_btn = document.getElementById('post-btn');
        if (user) {
            loginUser=user;
            user_email = user.email;
            menu.innerHTML = "<a href='userpage.html' class='dropdown-item'>" + user.email + "</a><span class='dropdown-item' id='logout-btn'>登出</span>";
            post_btn.innerHTML='<a class="nav-link" href="post.html">發文</a>';
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('登出成功')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>登入</a>";
            post_btn.innerHTML='';
            //document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_title = document.getElementById('title');
    post_content = document.getElementById('content');
    post_btn.addEventListener('click', function () {
        if (post_content.value != "") {
            var newpostref = firebase.database().ref('posts').push();
            newpostref.set({
                user: loginUser.uid,
                email: user_email,
                class: $("input[name=class]:checked").val(),
                content: post_content.value,
                title: post_title.value
            });
            post_content.value = "";
            post_title.value = "";
            alert('發文成功');
        }

    });

}

window.onload = function () {
    init();
}