function init() {

    var loginUser;
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        var post_btn = document.getElementById('post-btn');
        if (user) {

            loginUser=user;
            user_email = user.email;
            menu.innerHTML = "<a href='userpage.html' class='dropdown-item'>" + user.email + "</a><span class='dropdown-item' id='logout-btn'>登出</span>";
            post_btn.innerHTML='<a class="nav-link" href="post.html">發文</a>';
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('登出成功')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>登入</a>";
            post_btn.innerHTML='';
            //document.getElementById('post_list').innerHTML = "";
        }
    });
/////////////////////顯示一樓
    var tid=getValue("tid");

    var title = document.getElementById('title');
    var author_content = document.getElementById('author_content');
    var author_name = document.getElementById('author_name');
    //pro_about = document.getElementById('pro_about');

    firebase.database().ref('/posts/' + tid).once('value').then(function(snapshot) {
        //讀標題
        title.innerHTML=
        "<h5 class='border-bottom border-gray pl-2 m-2'>["+snapshot.val().class+"] "+
        snapshot.val().title+"</h5>";
        //讀內文
        author_content.innerHTML=snapshot.val().content;
        //讀發文者的資料
        author_userid=snapshot.val().user;
        firebase.database().ref('/users/' + author_userid).once('value').then(function(snapshot2) {
            author_name.innerHTML=snapshot2.val().name;
        });        
        //讀頭貼
        var storageRef = firebase.storage().ref();
        storageRef.child(author_userid).getDownloadURL().then(function (url) {
            document.getElementById('author_pic').src = url;
        }).catch(function (error) {
            console.log(error.message);
        });
    });


/////////////////////////////存回文
    reply_btn = document.getElementById('reply_btn');
    reply_content = document.getElementById('reply_content');

    reply_btn.addEventListener('click', function () {
        if (reply_content.value != "") {
            var newrelyref = firebase.database().ref('/posts/' + tid+'/reply').push();
            newrelyref.set({
                user: loginUser.uid,
                content: reply_content.value,
            });
            reply_content.value = "";
            alert('回覆成功');
        }
    });

//////////////////////顯示回文
var replyRef = firebase.database().ref('/posts/' + tid+'/reply');
var total_reply = [];
var first_count = 0;
var second_count = 0;
replyRef.once('value')
    .then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childData = childSnapshot.val();

            total_reply[total_reply.length] = 
            '<pre class="m-15 p-3 bg-light box-shadow border border-gray">'+
            childSnapshot.val().content+'</pre>';

            first_count += 1;
        });
        document.getElementById('reply_list').innerHTML = total_reply.join('');

        //add listener
        replyRef.on('child_added', function (data) {
            second_count += 1;
            if (second_count > first_count) {
                var childData = data.val();
                
                total_reply[total_reply.length] = 
                '<pre class="m-15 p-3 bg-light box-shadow border border-gray">'+
                childSnapshot.val().content+'</pre>';
                
                document.getElementById('reply_list').innerHTML = total_reply.join('');
            }
        });
    })
    .catch(e => console.log(e.message));


/*

    var str_before_username = 
    "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 lh-125 border-gray'><strong class='d-block text-gray-dark'>";

    var str_after_content = "</p></div></div>\n";

    var replyRef = firebase.database().ref('/posts/' + tid+'/reply');
    var total_reply = [];
    var first_count = 0;
    var second_count = 0;

    replyRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {

                var childData = childSnapshot.val();

                //讀回覆的user的資料
                var reply_user=childSnapshot.val().user;
                firebase.database().ref('/users/' + reply_user).once('value').then(function(snapshot2) {
//console.log(snapshot2.val().name);
                total_reply[total_reply.length] = 
'<div class="my-4 p-3 bg-white rounded box-shadow"><div class="row m-6"><div class="col-sm-3" ><div class="my-2 p-3 box-shadow">'+
'放圖片'+'</div><div class="my-2 p-3 box-shadow">'+ snapshot2.val().name +
'</div></div><div class="col-sm-9 bg-light"><pre class="m-20 p-4">'+'放內容'+
'</pre></div></div></div>';
});
                first_count += 1;
                
            });
            console.log(total_reply);
            console.log(total_reply.join(''));
            document.getElementById('reply_list').innerHTML = total_reply.join('');

            //add listener
            replyRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    
                    //讀回覆的user的資料
                    var reply_user_=data.val().user;
                    firebase.database().ref('/users/' + reply_user_).once('value').then(function(snapshot3) {


                    total_reply[total_reply.length] = 
'<div class="my-4 p-3 bg-white rounded box-shadow"><div class="row m-6"><div class="col-sm-3" ><div class="my-2 p-3 box-shadow">'+
'放圖片'+'</div><div class="my-2 p-3 box-shadow">'+snapshot3.val().name+
'</div></div><div class="col-sm-9 bg-light"><pre class="m-20 p-4">'+'放內容'+
'</pre></div></div></div>';
});
console.log(total_reply);
console.log(total_reply.join(''));
                    document.getElementById('reply_list').innerHTML = total_reply.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
        
*/

}


function getValue(varname)
{
  var url = window.location.href;
  var qparts = url.split("?");
  if (qparts.length == 0){return "";}
  var query = qparts[1];
  var vars = query.split("&amp;");
  var value = "";
  for (i=0; i<vars.length; i++)
  {
    var parts = vars[i].split("=");
    if (parts[0] == varname)
    {
      value = parts[1];
      break;
    }
  }
  value = unescape(value);
  value.replace(/\+/g," ");
  return value;
}

window.onload = function () {
    init();
}